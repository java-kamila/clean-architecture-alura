package br.com.kamila.escola;

import br.com.kamila.escola.gamificacao.aplicacao.GeraSeloAlunoNovato;
import br.com.kamila.escola.gamificacao.infra.selo.RepositorioSeloEmMemoria;
import br.com.kamila.escola.shared.dominio.evento.PublicadorDeEventos;
import br.com.kamila.escola.academico.dominio.aluno.LogDeAlunoMatriculado;
import br.com.kamila.escola.academico.dominio.aluno.RepositorioAluno;
import br.com.kamila.escola.academico.aplicacao.aluno.MatricularAluno;
import br.com.kamila.escola.academico.aplicacao.aluno.MatricularAlunoDto;
import br.com.kamila.escola.academico.infra.aluno.RepositorioAlunoEmMemoria;

public class MatricularAlunoPorLinhaDeComando {

    public static void main(String[] args) {
        String nome = "Fulano";
        String cpf = "123.456.789-00";
        String email = "fulano@email.com";

        PublicadorDeEventos publicador = new PublicadorDeEventos();
        publicador.adicionar(new LogDeAlunoMatriculado());
        publicador.adicionar(new GeraSeloAlunoNovato(
                new RepositorioSeloEmMemoria()
        ));

        RepositorioAluno repositorio = new RepositorioAlunoEmMemoria();
        MatricularAluno matricular = new MatricularAluno(repositorio, publicador);

        matricular.executa(
                new MatricularAlunoDto(nome, cpf, email)
        );
    }
}
