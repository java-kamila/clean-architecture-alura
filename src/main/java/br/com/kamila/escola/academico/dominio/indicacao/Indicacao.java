package br.com.kamila.escola.academico.dominio.indicacao;

import br.com.kamila.escola.academico.dominio.aluno.Aluno;

import java.time.LocalDateTime;

public class Indicacao {

    private Aluno alunoEfetivo;
    private Aluno alunoIndicado;
    private LocalDateTime dataIndicacao;

    public Indicacao(Aluno alunoEfetivo, Aluno alunoIndicado, LocalDateTime dataIndicacao) {
        this.alunoEfetivo = alunoEfetivo;
        this.alunoIndicado = alunoIndicado;
        this.dataIndicacao = dataIndicacao;
    }

    public Aluno getAlunoEfetivo() {
        return alunoEfetivo;
    }

    public Aluno getAlunoIndicado() {
        return alunoIndicado;
    }

    public LocalDateTime getDataIndicacao() {
        return dataIndicacao;
    }
}
