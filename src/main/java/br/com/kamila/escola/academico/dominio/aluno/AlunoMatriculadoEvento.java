package br.com.kamila.escola.academico.dominio.aluno;

import br.com.kamila.escola.shared.dominio.evento.Evento;
import br.com.kamila.escola.shared.dominio.CPF;
import br.com.kamila.escola.shared.dominio.evento.TipoDeEvento;

import java.time.LocalDateTime;
import java.util.Map;

// Eventos
public class AlunoMatriculadoEvento implements Evento {

    // final para obrigatar a inicialização do objeto na declaração ou no constructor
    private final CPF cpfDoAluno;
    private final LocalDateTime momento;

    public AlunoMatriculadoEvento(CPF cpfDoAluno) {
        // Apenas cpf do aluno evitando expor outras informações para a classe
        this.cpfDoAluno = cpfDoAluno;
        this.momento = LocalDateTime.now();
    }

    @Override
    public LocalDateTime momento() {
        return this.momento;
    }

    @Override
    public TipoDeEvento tipo() {
        return TipoDeEvento.ALUNO_MATRICULADO;
    }

    @Override
    public Map<String, Object> informacoes() {
        return Map.of("cpf", cpfDoAluno);
    }

    public CPF getCpfDoAluno() {
        return cpfDoAluno;
    }

}
