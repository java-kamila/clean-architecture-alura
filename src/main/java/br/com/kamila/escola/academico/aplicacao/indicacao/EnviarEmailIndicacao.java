package br.com.kamila.escola.academico.aplicacao.indicacao;

import br.com.kamila.escola.academico.dominio.aluno.Aluno;

public interface EnviarEmailIndicacao {

    void enviarPara(Aluno indicado);

}
