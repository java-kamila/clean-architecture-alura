package br.com.kamila.escola.academico.infra.aluno;

import br.com.kamila.escola.academico.dominio.aluno.AlunoNaoEncontrado;
import br.com.kamila.escola.academico.dominio.aluno.RepositorioAluno;
import br.com.kamila.escola.academico.dominio.aluno.Aluno;
import br.com.kamila.escola.shared.dominio.CPF;

import java.util.ArrayList;
import java.util.List;

public class RepositorioAlunoEmMemoria implements RepositorioAluno {

    private List<Aluno> matriculados = new ArrayList<>();

    @Override
    public void matricular(Aluno aluno) {
        this.matriculados.add(aluno);
    }

    @Override
    public Aluno buscarPorCpf(CPF cpf) {
        return this.matriculados.stream()
                .filter(a -> a.getCpf().equals(cpf.getNumero()))
                .findFirst()
                .orElseThrow(() -> new AlunoNaoEncontrado(cpf));
    }

    @Override
    public List<Aluno> listarTodosAlunosMatriculados() {
        return this.matriculados;
    }
}
