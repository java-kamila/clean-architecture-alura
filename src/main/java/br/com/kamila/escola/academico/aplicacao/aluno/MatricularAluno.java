package br.com.kamila.escola.academico.aplicacao.aluno;

import br.com.kamila.escola.shared.dominio.evento.PublicadorDeEventos;
import br.com.kamila.escola.academico.dominio.aluno.AlunoMatriculadoEvento;
import br.com.kamila.escola.academico.dominio.aluno.RepositorioAluno;
import br.com.kamila.escola.academico.dominio.aluno.Aluno;

import java.sql.SQLException;

public class MatricularAluno {

    private final RepositorioAluno repositorio;
    private final PublicadorDeEventos publicador;

    public MatricularAluno(RepositorioAluno repositorio, PublicadorDeEventos publicador) {
        this.repositorio = repositorio;
        this.publicador = publicador;
    }

    // Comand
    public void executa(MatricularAlunoDto aluno) {
        try {
            Aluno novo = aluno.criarAluno();
            repositorio.matricular(novo);

            AlunoMatriculadoEvento evento = new AlunoMatriculadoEvento(novo.getCpf());
            publicador.publicar(evento);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
