package br.com.kamila.escola.academico.dominio.aluno;

import br.com.kamila.escola.shared.dominio.CPF;

import java.util.ArrayList;
import java.util.List;

// ENTIDADE
// Agregate Root
public class Aluno {

    private CPF cpf;
    private String nome;
    private Email email;
    private List<Telefone> telefones = new ArrayList<>();

    private String senha;

    public Aluno(CPF cpf, String nome, Email email) {
        this.cpf = cpf;
        this.nome = nome;
        this.email = email;
    }

    /*
     Evitando que código externo adicione telefone, concentrando manipulação das informações dentro do
     Agregate Root.
     */
    public void adicionarTelefone(String ddd, String numero) {
        if (telefones.size() == 2) {
            throw new LimiteDeTelefonesException(numero);
        }
        this.telefones.add(new Telefone(ddd, numero));
    }

    public CPF getCpf() {
        return cpf;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email.getEndereco();
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }
}
