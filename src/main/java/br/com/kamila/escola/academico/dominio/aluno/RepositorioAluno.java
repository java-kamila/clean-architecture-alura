package br.com.kamila.escola.academico.dominio.aluno;

import br.com.kamila.escola.shared.dominio.CPF;

import java.sql.SQLException;
import java.util.List;

public interface RepositorioAluno {

    void matricular(Aluno aluno) throws SQLException;

    Aluno buscarPorCpf(CPF cpf);

    List<Aluno> listarTodosAlunosMatriculados();

}
