package br.com.kamila.escola.academico.dominio.aluno;

public class LimiteDeTelefonesException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public LimiteDeTelefonesException(String telefone) {
        super(String.format("Não foi possível salvar o telefone %s. Numero máximo atingido.", telefone));
    }

}
