package br.com.kamila.escola.academico.dominio.aluno;

import br.com.kamila.escola.shared.dominio.evento.Ouvinte;
import br.com.kamila.escola.shared.dominio.evento.Evento;
import br.com.kamila.escola.shared.dominio.evento.TipoDeEvento;

import java.time.format.DateTimeFormatter;

// Herança
public class LogDeAlunoMatriculado extends Ouvinte {

    public void reageAoEvento(Evento evento) {
        String momentoFormatado = evento
                .momento()
                .format(DateTimeFormatter
                        .ofPattern("dd/MM/yyy HH:mm"));

        System.out.println(
                String.format("Aluno com CPF %s matriculado em: %s",
                        ((AlunoMatriculadoEvento) evento).getCpfDoAluno(),
                        momentoFormatado)
        );
    }

    @Override
    protected boolean deveProcessar(Evento evento) {
        return evento.tipo() == TipoDeEvento.ALUNO_MATRICULADO;
    }

}
