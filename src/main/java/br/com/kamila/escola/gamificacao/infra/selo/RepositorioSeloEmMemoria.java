package br.com.kamila.escola.gamificacao.infra.selo;

import br.com.kamila.escola.shared.dominio.CPF;
import br.com.kamila.escola.gamificacao.dominio.selo.RepositorioSelo;
import br.com.kamila.escola.gamificacao.dominio.selo.Selo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RepositorioSeloEmMemoria implements RepositorioSelo {

    private List<Selo> selos = new ArrayList<>();

    @Override
    public void adicionar(Selo selo) {
        selos.add(selo);
    }

    @Override
    public List<Selo> buscarSelosDoAluno(CPF cpf) {
        return this.selos
                .stream()
                .filter(s -> s.getCpfDoAluno().equals(cpf.getNumero()))
                .collect(Collectors.toList());
    }
}
