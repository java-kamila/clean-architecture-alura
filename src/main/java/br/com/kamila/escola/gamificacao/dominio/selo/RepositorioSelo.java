package br.com.kamila.escola.gamificacao.dominio.selo;

import br.com.kamila.escola.shared.dominio.CPF;

import java.util.List;

public interface RepositorioSelo {

    void adicionar(Selo selo);

    List<Selo> buscarSelosDoAluno(CPF cpf);
}
