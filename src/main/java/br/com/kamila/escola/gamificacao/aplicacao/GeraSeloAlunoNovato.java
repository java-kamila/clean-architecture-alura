package br.com.kamila.escola.gamificacao.aplicacao;

import br.com.kamila.escola.gamificacao.dominio.selo.RepositorioSelo;
import br.com.kamila.escola.gamificacao.dominio.selo.Selo;
import br.com.kamila.escola.shared.dominio.CPF;
import br.com.kamila.escola.shared.dominio.evento.Evento;
import br.com.kamila.escola.shared.dominio.evento.Ouvinte;
import br.com.kamila.escola.shared.dominio.evento.TipoDeEvento;

public class GeraSeloAlunoNovato extends Ouvinte {

    private final RepositorioSelo repositorioSelo;

    public GeraSeloAlunoNovato(RepositorioSelo repositorioSelo) {
        this.repositorioSelo = repositorioSelo;
    }

    @Override
    protected void reageAoEvento(Evento evento) {
        CPF cpfDoAluno = (CPF) evento.informacoes().get("cpf");
        Selo novato = new Selo(cpfDoAluno, "Novato");

        repositorioSelo.adicionar(novato);
    }

    @Override
    protected boolean deveProcessar(Evento evento) {
        return evento.tipo() == TipoDeEvento.ALUNO_MATRICULADO;
    }
}
