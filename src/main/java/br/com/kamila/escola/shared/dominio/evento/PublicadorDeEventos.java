package br.com.kamila.escola.shared.dominio.evento;

import java.util.ArrayList;
import java.util.List;

public class PublicadorDeEventos {

    private List<Ouvinte> ouvintes = new ArrayList<>();

    /**
     * Adiciona listenner para um evento
     * @param ouvinte, classe que estende Ouvinte
     */
    public void adicionar(Ouvinte ouvinte) {
        this.ouvintes.add(ouvinte);
    }

    public void publicar(Evento evento) {
        this.ouvintes.forEach(o -> {
            o.processa(evento);
        });
    }

}
