package br.com.kamila.escola.shared.dominio.evento;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

public interface Evento {

    LocalDateTime momento();

    TipoDeEvento tipo();

    Map<String, Object> informacoes();

}
