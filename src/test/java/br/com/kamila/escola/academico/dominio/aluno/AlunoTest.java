package br.com.kamila.escola.academico.dominio.aluno;

import br.com.kamila.escola.shared.dominio.CPF;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AlunoTest {

    private Aluno aluno;

    @BeforeEach
    void beforeEach() {
        this.aluno = new Aluno(
                new CPF("123.456.789-00"),
                "Ricardo",
                new Email("ricardo@email.com")
        );
    }

    @Test
    void devePermitirAdicionarUmTelefone() {
        this.aluno.adicionarTelefone("85", "11112222");
        assertEquals(1, this.aluno.getTelefones().size());
    }

    @Test
    void devePermitirAdicionarDoisTelefones() {
        this.aluno.adicionarTelefone("85", "11112222");
        this.aluno.adicionarTelefone("11", "11112222");
        assertEquals(2, this.aluno.getTelefones().size());
    }

    @Test
    void deveFalharQuandoSalvarMaisDeDoisTelefonesPorAluno() {
        assertThrows(LimiteDeTelefonesException.class,
                () -> {
                    aluno.adicionarTelefone("85", "11112222");
                    aluno.adicionarTelefone("85", "22223333");
                    aluno.adicionarTelefone("85", "33334444");
                });

    }
}