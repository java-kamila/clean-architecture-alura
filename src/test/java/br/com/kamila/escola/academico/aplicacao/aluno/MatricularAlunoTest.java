package br.com.kamila.escola.academico.aplicacao.aluno;

import br.com.kamila.escola.shared.dominio.evento.PublicadorDeEventos;
import br.com.kamila.escola.academico.dominio.aluno.Aluno;
import br.com.kamila.escola.shared.dominio.CPF;
import br.com.kamila.escola.academico.dominio.aluno.LogDeAlunoMatriculado;
import br.com.kamila.escola.dominio.aluno.*;
import br.com.kamila.escola.academico.infra.aluno.RepositorioAlunoEmMemoria;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatricularAlunoTest {

    @Test
    void alunoDeveSerPersistidoQuandoSalvar() {
        RepositorioAlunoEmMemoria repositorio = new RepositorioAlunoEmMemoria();
        PublicadorDeEventos publicador = new PublicadorDeEventos();
        publicador.adicionar(new LogDeAlunoMatriculado());

        MatricularAluno useCase = new MatricularAluno(repositorio, publicador);

        MatricularAlunoDto dados = new MatricularAlunoDto(
                "Fulano",
                "123.456.789-00",
                "fulano@email.com");
        useCase.executa(dados);

        Aluno alunoEncontrado = repositorio.buscarPorCpf(new CPF("123.456.789-00"));
        assertEquals("Fulano", alunoEncontrado.getNome());
        assertEquals("123.456.789-00", alunoEncontrado.getCpf());
        assertEquals("fulano@email.com", alunoEncontrado.getEmail());
    }

}