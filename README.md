# Escola (Clean Architecture e DDD)

Projeto de estudo sobre Clean Architecture e Domain Driven Design realizado em curso da plataforma Alura.
Módulso do projeto: infraestrutura, aplicação (usecases), domínio.

## Clean Architecture
<img src="https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg">

#### Observações

`Domain-Driven Design`, design orientado a domínio é uma abordagem ao desenvolvimento de software que centraliza o desenvolvimento na programação de um modelo de domínio que possui uma rica compreensão dos processos e regras de um negócio.<br>
Um atributo `Long Id`, por exemplo, não é um conceito do domínio da aplicação, mas sim de infraestrutura, portanto não deve estar em uma entidade.
As camadas mais externas podem depender de camadas mais internas, assim o domínio fica independente de alterações exteriores.
As camadas mais internas não devem depender de camadas externas.
A estrutura de módulos fornece maior **manutenibilidade** e **extensibilidade** para a aplicação.

- Domain Services são classes que representam uma ação entre mais de uma entidade.
- Application Services controlam o fluxo de alguma regra em nossa aplicação.
- Infrastructure Services são implementações de interfaces presentes nas camadas de domínio ou de aplicação.

Os termos Use Case, Application Service e Command Handler são basicamente sinônimos e servem para fornecer pontos de entrada na sua aplicação, de forma independente dos mecanismos de entrega (Web, CLI, etc).
Arquitetura recomendada pra projetos complexos, com muitas regras de negócio.

Vantagens: 
 - Isolar o código de domínio da aplicação
 - Inversão de dependências
 - Favorece boas práticas de Orientação a Objeto
 - Código de domínio com alta "testabilidade"
 - Independência de frameworks e detalhes de infraestrutura

Desvantagens:
 - Alto número de classes, arquivos.

#### Value Object
Entidades possuem uma identidade única, enquanto `VOs` são considerados iguais, se todos os seus atributos tiverem valores iguais.<br>
Por exemplo, se dois e-mails possuem o mesmo endereço, podemos considerá-los como o mesmo e-mail.
Já duas pessoas com o nome, altura e idade não necessariamente são a mesma pessoa.<br>
Documento com descrição sobre [o que é um Value Object](https://martinfowler.com/bliki/ValueObject.html).

#### Entities
Descreve as regras de negócio corporativas ou os objetos de negócio da aplicação, encapsulando as regras mais gerais e de alto nível.
Eles são as classes menos propensas a mudar quando há mudanças externas.

-----
## Domain Driven Design
"Desenvolver software orientado a domínio". Modelar software orientando-se pelo domínio do negócio.<br>
Linguagem ubíqua, linguagem comum entre equipe de domínio, que detém regras de negócio e equipe técnica, utilizar termos conhecidos pelo negócio.
Facilita a comunicação, compreensão e manutenção do código.<br>
Contexto delimitado.<br>

Building blocks ou Blocos de construção:
     - Entity
     - Value Object
     - Repository
     - Factory
     - Service

`Invariantes` são regras de negócio que precisam ser verificadas para garantir sua consistência. <br>
Entidade com conjunto de Value Objects é chamada de `Agregate Root`, possui uma coleção, no projeto seria a classe Aluno. <br>
Artigo do Martin Fowler sobre [Agregate](https://martinfowler.com/bliki/DDD_Aggregate.html).

Um `evento` pode chamar várias ações, fornecendo flexibilidade.

#### Bounded Contexts
A vantagem de separar nossa aplicação em contextos delimitados (`Bounded Contexts`) é a flexibilidade, 
podemos ter equipes separadas para trabalhar em cada um dos contextos
e além disso eles podem até virar projetos separados, evoluindo individualmente.
Vale ressaltar que aumenta a complexidade.<br>
Neste projeto temos os contextos: academico e gamificacao.

#### Shared Kernel

A utilização de um núcleo compartilhado tem suas vantagens e desvantagens.

É a forma mais fácil de possibilitar a comunicação entre contextos delimitados, mas nos tira boa parte da flexibilidade.

Aqui nesse artigo há uma revisão sobre Bounded Contexts e uma breve citação sobre Shared Kernel: http://www.fabriciorissetto.com/blog/ddd-bounded-context/

#### Sugestões de leitura

 - Livro Domain-Driven Design, Eric Evans. Abordagem conceitual.
 - Livro Domain-Driven Design Distilled, Vaughn Vernon. ABordagem conceitual mais resumida.
 - Livro Implementing Domain-Driven Design, Vaughn Vernon. Abordagem mais prática.
 - Livro Domain-Driven Design with Spring Boot, Ajay Kumar. Abordagem mais técnica prática.